from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list": todolist,
    }
    return render(request, "todos/list.html", context)


def show_todos(request, id):
    todos_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todos_list,
    }
    return render(request, "todos/details.html", context)


def create_list_item(request):
    if request.method == "POST":
        itemform = TodoItemForm(request.POST)
        if itemform.is_valid():
            item = itemform.save()
            item.save()
        return redirect("todo_list_detail", id=item.list.id)
    else:
        itemform = TodoItemForm()
    context = {
        "itemform": itemform
    }
    return render(request, "todos/create_item.html", context)


def item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        itemform = TodoItemForm(request.POST, instance=item)
        if itemform.is_valid():
            items = itemform.save()
            items.save()
        return redirect("todo_list_detail", id=item.list.id)
    else:
        itemform = TodoItemForm(instance=item)
    context = {
        "item": item,
        "itemform": itemform
    }
    return render(request, "todos/item_update.html", context)


def create_new_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            list.save()
        return redirect("todo_list_detail", list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            list.save()
        return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)

    context = {
        "list": list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")
