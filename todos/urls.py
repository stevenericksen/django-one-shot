from django.urls import path
from todos.views import todo_list, show_todos, create_new_list, edit_list
from todos.views import delete_list, create_list_item, item_update


urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", show_todos, name="todo_list_detail"),
    path("items/create/", create_list_item, name="todo_item_create"),
    path("<int:id>/edit/", edit_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("create/", create_new_list, name="todo_list_create"),
    path("items/<int:id>/edit/", item_update, name="todo_item_update")
]
